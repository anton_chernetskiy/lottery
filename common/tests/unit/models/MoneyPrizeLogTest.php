<?php

namespace common\tests\unit\models;

use common\fixtures\MoneyPrizeLogFixture;
use common\models\User;
use Yii;
use common\models\prize\log\MoneyPrizeLog;
use common\fixtures\UserFixture;

/**
 * Login form test
 */
class MoneyPrizeLogTest extends \Codeception\Test\Unit
{
    /**
     * @var \common\tests\UnitTester
     */
    protected $tester;


    /**
     * @return array
     */
    public function _fixtures()
    {
        return [
            'user' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'user.php'
            ],
            'money' => [
                'class' => MoneyPrizeLogFixture::className(),
                'dataFile' => codecept_data_dir() . 'money.php'
            ]
        ];
    }

    public function testConvert()
    {
        $money = MoneyPrizeLog::findOne(['id' => '1']);
        $user = User::findOne(['username' => $money->user_id]);

        $money->convert();

        $bonus_point_balance = $user->bonus_point_balance + $money->amount * Yii::$app->params['moneyPrize.convertCoefficient'];

        $user = User::findOne(['username' => $money->user_id]);

        $this->assertEquals($money->status, MoneyPrizeLog::STATUS_CONVERTED);
        $this->assertEquals($user->bonus_point_balance, $bonus_point_balance);
    }
}
