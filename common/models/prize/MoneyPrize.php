<?php
namespace common\models\prize;

use common\models\prize\log\MoneyPrizeLog;
use common\models\prize\log\PrizeLogInterface;
use Yii;
/**
 * MoneyPrize model
 *
 * @property integer $id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $lottery_id
 * @property double $amount_from
 * @property double $amount_to
 * @property double $amount
 * @property double $balance
 */
class MoneyPrize extends Prize implements PrizeInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%money_prize}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['lottery_id', 'required'],
            ['lottery_id', 'integer'],

            ['amount_from', 'double'],

            ['amount_to', 'double'],

            ['amount', 'double'],

            ['balance', 'double'],
        ];
    }

    public static function getByLotteryId($lottery_id, $user_id):PrizeLogInterface
    {
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();

        try {
            $sql = self::find()
                ->andWhere('lottery_id=:lottery_id AND balance>=amount_from AND amount>0', [':lottery_id' => $lottery_id])
                ->createCommand()
                ->getRawSql();

            $prize = self::findBySql($sql . ' FOR UPDATE')->one();

            if( $prize === null ){
                throw new \Exception('No prize available');
            }

            $log = self::getLogger()->write($prize, $lottery_id, $user_id);

            $prize->balance = $prize->balance - $log->amount;
            $prize->save();

            $transaction->commit();

            return $log;
        }catch(\Throwable $e) {
            $transaction->rollBack();

            throw new \Exception('Can not save information to DB. Error: '.$e->getMessage());
        }
    }

    public static function getLogger():PrizeLogInterface
    {
        return new MoneyPrizeLog();
    }
}
