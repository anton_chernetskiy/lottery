<?php
namespace common\models\prize;

interface PrizeInterface
{
    public static function getByLotteryId($lottery_id, $user_id);

    public static function getLogger();
}