<?php
namespace common\models\prize;

use common\models\prize\log\PrizeLogInterface;
use common\models\prize\log\BonusPointPrizeLog;
use Yii;
/**
 * BonusPointPrize model
 *
 * @property integer $id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $lottery_id
 * @property integer $amount_from
 * @property integer $amount_to
 */

class BonusPointPrize extends Prize implements PrizeInterface
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%bonus_point_prize}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['lottery_id', 'required'],
            ['lottery_id', 'integer'],

            ['amount_from', 'integer'],

            ['amount_to', 'integer'],
        ];
    }

    public static function getByLotteryId($lottery_id, $user_id):PrizeLogInterface
    {
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();

        try {
            $sql = self::find()
                ->andWhere('lottery_id=:lottery_id AND amount_from>0 AND amount_to>0', [':lottery_id' => $lottery_id])
                ->createCommand()
                ->getRawSql();

            $prize = self::findBySql($sql . ' FOR UPDATE')->one();

            if( $prize === null ){
                throw new \Exception('No prize available');
            }

            $log = self::getLogger()->write($prize, $lottery_id, $user_id);

            $transaction->commit();

            return $log;
        }catch(\Throwable $e) {
            $transaction->rollBack();

            throw new \Exception('Can not save information to DB. Error: '.$e->getMessage());
        }
    }

    public static function getLogger():PrizeLogInterface
    {
        return new BonusPointPrizeLog();
    }
}
