<?php
namespace common\models\prize\log;

use common\models\prize\PrizeInterface;

interface PrizeLogInterface
{
    public function getName();
    public function write(PrizeInterface $model, $lottery_id, $user_id);

    public function convert():bool;
    public function cancel():bool;
    public function payout():bool;
}