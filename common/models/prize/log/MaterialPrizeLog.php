<?php
namespace common\models\prize\log;

use common\models\prize\MaterialPrize;
use common\models\prize\PrizeInterface;
use Yii;

/**
 * MaterialPrizeLog model
 *
 * @property integer $id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $lottery_id
 * @property integer $material_prize_id
 * @property integer $user_id
 * @property integer $amount
 * @property string $status
 */

class MaterialPrizeLog extends PrizeLog implements PrizeLogInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%material_prize_log}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['lottery_id', 'required'],
            ['lottery_id', 'integer'],

            ['material_prize_id', 'required'],
            ['material_prize_id', 'integer'],

            ['user_id', 'required'],
            ['user_id', 'integer'],

            ['amount', 'integer'],

            ['status', 'default', 'value' => self::STATUS_NEW],
            ['status', 'in', 'range' => array_keys(self::getStatuses())],
        ];
    }

    public function getName()
    {
        $prize = MaterialPrize::findOne($this->material_prize_id);

        return $this->amount.' x '.$prize->name;
    }

    public function payout():bool
    {
        $this->status = self::STATUS_PAID_OUT;

        if( $this->save() ){
            //Send to user by post
            $this->postSend($this);

            return true;
        }

        return false;
    }

    public function cancel():bool
    {
        $this->status = self::STATUS_REFUSED;
        return $this->save();
    }

    public function convert():bool
    {
        throw new \Exception('Not allowed');
    }

    public function write(PrizeInterface $model, $lottery_id, $user_id):PrizeLogInterface
    {
        $this->lottery_id = $lottery_id;
        $this->material_prize_id = $model->id;
        $this->user_id = $user_id;
        $this->amount = 1;
        $this->save();

        return $this;
    }

    public function postSend(MaterialPrizeLog $model){
        return true;
    }
}
