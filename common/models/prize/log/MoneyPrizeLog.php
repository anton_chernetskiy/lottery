<?php
namespace common\models\prize\log;

use common\models\prize\PrizeInterface;
use common\models\User;
use Yii;
use yii\db\Expression;

/**
 * MoneyPrizeLog model
 *
 * @property integer $id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $lottery_id
 * @property integer $money_prize_id
 * @property integer $user_id
 * @property double $amount
 * @property string $status
 */

class MoneyPrizeLog extends PrizeLog implements PrizeLogInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%money_prize_log}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['lottery_id', 'required'],
            ['lottery_id', 'integer'],

            ['money_prize_id', 'required'],
            ['money_prize_id', 'integer'],

            ['user_id', 'required'],
            ['user_id', 'integer'],

            ['amount', 'integer'],

            ['status', 'default', 'value' => self::STATUS_NEW],
            ['status', 'in', 'range' => array_keys(self::getStatuses())],
        ];
    }

    public function getName()
    {
        return 'Money '.number_format($this->amount, 2, '.', '.');
    }

    public function cancel():bool
    {
        $this->status = self::STATUS_REFUSED;
        return $this->save();
    }

    public function payout():bool
    {
        if( $this->httpRequestToBank() !== 'success' ){
            throw new \Exception('Bank response error');
        }

        $this->status = self::STATUS_PAID_OUT;
        return $this->save();
    }

    public function convert():bool
    {
        $convertCoefficient = Yii::$app->params['moneyPrize.convertCoefficient'];

        $this->status = self::STATUS_CONVERTED;
        if( $this->save() ){
            $user = User::findOne($this->user_id);
            $user->bonus_point_balance = new \yii\db\Expression('bonus_point_balance + '.($this->amount * $convertCoefficient));
            $user->save();

            return true;
        }

        return false;
    }

    public function write(PrizeInterface $model, $lottery_id, $user_id):PrizeLogInterface
    {
        $k = 1000000;

        $amount_to = $model->balance > $model->amount_to ? $model->amount_to : $model->balance;

        $this->lottery_id = $lottery_id;
        $this->money_prize_id = $model->id;
        $this->user_id = $user_id;
        $this->amount = mt_rand($k*$model->amount_from, $k*$amount_to)/$k;
        $this->save();

        return $this;
    }

    public function httpRequestToBank(){
        return 'success';
    }
}
