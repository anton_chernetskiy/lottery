<?php
namespace common\models\prize\log;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;


class PrizeLog extends ActiveRecord
{
    const STATUS_NEW = 'new';
    const STATUS_CONVERTED = 'converted';
    const STATUS_PAID_OUT = 'paid_out';
    const STATUS_REFUSED = 'refused';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public static function getStatuses():array
    {
        return [
            self::STATUS_NEW => 'New',
            self::STATUS_CONVERTED => 'Converted',
            self::STATUS_PAID_OUT => 'Paid out',
            self::STATUS_REFUSED => 'Refused',
        ];
    }
}
