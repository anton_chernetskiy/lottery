<?php
namespace common\models\prize\log;

use common\models\prize\PrizeInterface;
use common\models\User;
use Yii;

/**
 * BonusPointPrizeLog model
 *
 * @property integer $id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $lottery_id
 * @property integer $bonus_point_prize_id
 * @property integer $user_id
 * @property integer $amount
 * @property string $status
 */

class BonusPointPrizeLog extends PrizeLog implements PrizeLogInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%bonus_point_prize_log}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['lottery_id', 'required'],
            ['lottery_id', 'integer'],

            ['bonus_point_prize_id', 'required'],
            ['bonus_point_prize_id', 'integer'],

            ['user_id', 'required'],
            ['user_id', 'integer'],

            ['amount', 'integer'],

            ['status', 'default', 'value' => self::STATUS_NEW],
            ['status', 'in', 'range' => array_keys(self::getStatuses())],
        ];
    }

    public function getName()
    {
        return 'Bonus points '.$this->amount;
    }

    public function payout():bool
    {
        //Send to user balance
        $this->status = self::STATUS_PAID_OUT;
        if( $this->save() ){
            \Yii::$app->db->createCommand()
                ->update(User::tableName(), ['bonus_point_balance' => new \yii\db\Expression('bonus_point_balance + '.$this->amount)], 'id=:id', [':id'=>$this->user_id])
                ->execute();

            return true;
        }

        return false;
    }

    public function cancel():bool
    {
        $this->status = self::STATUS_REFUSED;
        return $this->save();
    }

    public function convert():bool
    {
        throw new \Exception('Not allowed');
    }

    public function write(PrizeInterface $model, $lottery_id, $user_id):PrizeLogInterface
    {
        $this->lottery_id = $lottery_id;
        $this->bonus_point_prize_id = $model->id;
        $this->user_id = $user_id;
        $this->amount = mt_rand($model->amount_from, $model->amount_to);
        $this->save();

        return $this;
    }
}
