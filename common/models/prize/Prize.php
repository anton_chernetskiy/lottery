<?php
namespace common\models\prize;

use common\models\Lottery;
use common\models\prize\log\PrizeLogInterface;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class Prize extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public static function getRandom($user_id, Lottery $lottery, array $modelPrizes = []):PrizeLogInterface
    {
        shuffle($modelPrizes);

        foreach ($modelPrizes as $modelPrize){
            try{
                $prizeLog = $modelPrize->getByLotteryId($lottery->id, $user_id);

                if( $prizeLog !== null ){
                    return $prizeLog;
                }
            }catch (\Throwable $e){
                throw new \Exception($e->getMessage());
            }
        }

        throw new \Exception('Prize not found');
    }
}
