<?php
namespace common\models;

use common\models\prize\BonusPointPrize;
use common\models\prize\log\BonusPointPrizeLog;
use common\models\prize\log\MaterialPrizeLog;
use common\models\prize\log\MoneyPrizeLog;
use common\models\prize\log\PrizeLogInterface;
use common\models\prize\MaterialPrize;
use common\models\prize\MoneyPrize;
use common\models\prize\Prize;
use common\models\prize\PrizeInterface;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Exception;

/**
 * Lottery model
 *
 * @property integer $id
 * @property string $created_at
 * @property string $updated_at
 * @property string $name
 */
class Lottery extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%lottery}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'string', 'min' => 2, 'max' => 255],
        ];
    }

    public static function getRandomPrizeByUserId($user_id):PrizeLogInterface
    {
        $lockName = 'lottery_user_'.$user_id;

        $mutex = Yii::$app->mutex->acquire($lockName);

        if ($mutex) {
            try {
                $prizeLog = Prize::getRandom($user_id, self::getRandomByUserId($user_id), [
                    new MoneyPrize(), new BonusPointPrize(), new MaterialPrize()
                ]);

                Yii::$app->mutex->release($lockName);

                return $prizeLog;
            }catch(\Throwable $e) {
                Yii::$app->mutex->release($lockName);

                throw new Exception('Get random prize error: '.$e->getMessage());
            }
        } else {
            throw new Exception('At the moment you are participating in the lottery. Try again later.');
        }
    }

    public static function getRandomByUserId($user_id):self
    {
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();

        try {
            $sql = self::find()
                ->andWhere('id NOT IN (
                        SELECT lottery_id FROM '.MoneyPrizeLog::tableName().' WHERE user_id=:user_id UNION DISTINCT
                        SELECT lottery_id FROM '.BonusPointPrizeLog::tableName().' WHERE user_id=:user_id UNION DISTINCT
                        SELECT lottery_id FROM '.MaterialPrizeLog::tableName().' WHERE user_id=:user_id 
                    )', [':user_id' => $user_id])
                ->createCommand()
                ->getRawSql();;

            $lotteries = self::findBySql($sql . ' FOR UPDATE')->all();

            if( empty($lotteries) ){
                throw new \Exception('No lotteries available');
            }

            $transaction->commit();

            return $lotteries[array_rand($lotteries)];
        }catch(\Throwable $e) {
            $transaction->rollBack();

            throw new Exception('Can not save information to DB. Error: '.$e->getMessage());
        }
    }
}
