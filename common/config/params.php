<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'user.passwordResetTokenExpire' => 3600,
    'user.passwordMinLength' => 5,
    'moneyPrize.convertCoefficient' => 0.1,
    'console.batchPayoutCountByCycle' => 10,
];
