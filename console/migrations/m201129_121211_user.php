<?php

use yii\db\Migration;

/**
 * Class m201129_121211_user
 */
class m201129_121211_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'bonus_point_balance', 'INTEGER(11) NULL DEFAULT 0');

        $users = [
            [
                'username' => 'admin',
                'password' => 'admin',
                'email' => 'admin@test.com',
            ],
            [
                'username' => 'user1',
                'password' => 'user1',
                'email' => 'user1@test.com',
            ],
            [
                'username' => 'user2',
                'password' => 'user2',
                'email' => 'user2@test.com',
            ],
            [
                'username' => 'user3',
                'password' => 'user3',
                'email' => 'user3@test.com',
            ],
        ];

        foreach ($users as $user){
            $this->insert('user', [
                'username' => $user['username'],
                'email' => $user['email'],
                'auth_key' => Yii::$app->security->generateRandomString(),
                'password_hash' => Yii::$app->security->generatePasswordHash($user['password']),
                'password_reset_token' => null,
                'status' => 10,
                'created_at' => time(),
                'updated_at' => time(),
                'verification_token' => Yii::$app->security->generateRandomString() . '_' . time(),
                'bonus_point_balance' => 0,
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->truncateTable('user');

        $this->dropColumn('user', 'bonus_point_balance');
    }
}
