<?php

use yii\db\Migration;

/**
 * Class m201128_215116_lottery
 */
class m201128_215116_lottery extends Migration
{
    private const TABLE_NAME_LOTTERY = 'lottery';

    private const TABLE_NAME_MONEY_PRIZE = 'money_prize';
    private const TABLE_NAME_BONUS_PRIZE = 'bonus_point_prize';
    private const TABLE_NAME_MATERIAL_PRIZE = 'material_prize';

    private const TABLE_NAME_MONEY_PRIZE_LOG = 'money_prize_log';
    private const TABLE_NAME_BONUS_PRIZE_LOG = 'bonus_point_prize_log';
    private const TABLE_NAME_MATERIAL_PRIZE_LOG = 'material_prize_log';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME_LOTTERY, [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer(11)->null(),
            'updated_at' => $this->integer(11)->null(),
            'name' => $this->string(255)->null(),
        ]);

        $this->createTable(self::TABLE_NAME_MONEY_PRIZE, [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer(11)->null(),
            'updated_at' => $this->integer(11)->null(),
            'lottery_id' => $this->integer(11)->null(),
            'amount_from' => $this->double()->null(),
            'amount_to' => $this->double()->null(),
            'amount' => $this->double()->null(),
            'balance' => $this->double()->null(),
        ]);

        $this->createTable(self::TABLE_NAME_MONEY_PRIZE_LOG, [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer(11)->null(),
            'updated_at' => $this->integer(11)->null(),
            'lottery_id' => $this->integer(11)->null(),
            'money_prize_id' => $this->integer(11)->null(),
            'user_id' => $this->integer(11)->null(),
            'amount' => $this->double()->null(),
            'status' => $this->string(255)->null(),
        ]);

        $this->createTable(self::TABLE_NAME_BONUS_PRIZE, [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer(11)->null(),
            'updated_at' => $this->integer(11)->null(),
            'lottery_id' => $this->integer(11)->null(),
            'amount_from' => $this->integer(11)->null(),
            'amount_to' => $this->integer(11)->null(),
        ]);

        $this->createTable(self::TABLE_NAME_BONUS_PRIZE_LOG, [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer(11)->null(),
            'updated_at' => $this->integer(11)->null(),
            'lottery_id' => $this->integer(11)->null(),
            'bonus_point_prize_id' => $this->integer(11)->null(),
            'user_id' => $this->integer(11)->null(),
            'amount' => $this->integer()->null(),
            'status' => $this->string(255)->null(),
        ]);

        $this->createTable(self::TABLE_NAME_MATERIAL_PRIZE, [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer(11)->null(),
            'updated_at' => $this->integer(11)->null(),
            'lottery_id' => $this->integer(11)->null(),
            'name' => $this->string(255)->null(),
            'quantity' => $this->integer(11)->null(),
            'balance' => $this->integer(11)->null(),
        ]);

        $this->createTable(self::TABLE_NAME_MATERIAL_PRIZE_LOG, [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer(11)->null(),
            'updated_at' => $this->integer(11)->null(),
            'lottery_id' => $this->integer(11)->null(),
            'material_prize_id' => $this->integer(11)->null(),
            'user_id' => $this->integer(11)->null(),
            'amount' => $this->integer()->null(),
            'status' => $this->string(255)->null(),
        ]);

        foreach (self::getLotteryPrizeData() as $lotteryName => $lotteryPrizeData){
            $this->insert(self::TABLE_NAME_LOTTERY, [
                'created_at' => time(),
                'name' => $lotteryName,
            ]);

            $lottery_id = Yii::$app->db->getLastInsertID();

            foreach ($lotteryPrizeData as $table => $data){
                foreach ($data as $columns){
                    $columns['lottery_id'] = $lottery_id;
                    $columns['created_at'] = time();

                    $this->insert($table, $columns);
                }
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME_LOTTERY);

        $this->dropTable(self::TABLE_NAME_MONEY_PRIZE);
        $this->dropTable(self::TABLE_NAME_BONUS_PRIZE);
        $this->dropTable(self::TABLE_NAME_MATERIAL_PRIZE);

        $this->dropTable(self::TABLE_NAME_MONEY_PRIZE_LOG);
        $this->dropTable(self::TABLE_NAME_BONUS_PRIZE_LOG);
        $this->dropTable(self::TABLE_NAME_MATERIAL_PRIZE_LOG);
    }

    private static function getLotteryPrizeData():array
    {
        return [
            'Test lottery 1 (TV)' => [
                self::TABLE_NAME_MONEY_PRIZE => [
                    [
                        'amount_from' => 0,
                        'amount_to' => 100,
                        'amount' => 1000,
                        'balance' => 1000,
                    ]
                ],
                self::TABLE_NAME_BONUS_PRIZE => [
                    [
                        'amount_from' => 0,
                        'amount_to' => 10,
                    ]
                ],
                self::TABLE_NAME_MATERIAL_PRIZE => [
                    [
                        'name' => 'Samsung TV',
                        'quantity' => 3,
                        'balance' => 3,
                    ],
                    [
                        'name' => 'Apple TV',
                        'quantity' => 5,
                        'balance' => 5,
                    ],
                    [
                        'name' => 'LG TV',
                        'quantity' => 10,
                        'balance' => 10,
                    ],
                ]
            ],
            'Test lottery 2 (Car)' => [
                self::TABLE_NAME_MONEY_PRIZE => [
                    [
                        'amount_from' => 0,
                        'amount_to' => 5,
                        'amount' => 10,
                        'balance' => 10,
                    ]
                ],
                self::TABLE_NAME_BONUS_PRIZE => [
                    [
                        'amount_from' => 0,
                        'amount_to' => 1000,
                    ]
                ],
                self::TABLE_NAME_MATERIAL_PRIZE => [
                    [
                        'name' => 'BMW',
                        'quantity' => 3,
                        'balance' => 3,
                    ],
                    [
                        'name' => 'Audi',
                        'quantity' => 5,
                        'balance' => 5,
                    ],
                    [
                        'name' => 'Porsche',
                        'quantity' => 10,
                        'balance' => 10,
                    ],
                ]
            ],
            'Test lottery 3 (Phone)' => [
                self::TABLE_NAME_MONEY_PRIZE => [
                    [
                        'amount_from' => 200,
                        'amount_to' => 1000,
                        'amount' => 30000,
                        'balance' => 30000,
                    ]
                ],
                self::TABLE_NAME_BONUS_PRIZE => [
                    [
                        'amount_from' => 10,
                        'amount_to' => 50,
                    ]
                ],
                self::TABLE_NAME_MATERIAL_PRIZE => [
                    [
                        'name' => 'iPhone',
                        'quantity' => 3,
                        'balance' => 3,
                    ],
                    [
                        'name' => 'Samsung',
                        'quantity' => 5,
                        'balance' => 5,
                    ],
                    [
                        'name' => 'Huawei',
                        'quantity' => 10,
                        'balance' => 10,
                    ],
                ]
            ],
        ];
    }
}
