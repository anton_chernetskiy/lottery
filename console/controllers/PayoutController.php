<?php
namespace console\controllers;

use common\models\prize\log\MoneyPrizeLog;
use yii\console\Controller;
use Yii;

class PayoutController extends Controller
{
    public function actionExecute()
    {
        $n = Yii::$app->params['console.batchPayoutCountByCycle'];

        while ($n > 0){
            $db = Yii::$app->db;
            $transaction = $db->beginTransaction();

            try {
                $sql = MoneyPrizeLog::find()
                    ->andWhere('status=:status', [':status' => MoneyPrizeLog::STATUS_NEW])
                    ->limit(1)
                    ->createCommand()
                    ->getRawSql();

                $prize = MoneyPrizeLog::findBySql($sql . ' FOR UPDATE')->one();

                if( $prize === null ){
                    break;
                }

                if( $prize->payout() === false ){
                    throw new \Exception('Payout fail');
                };

                $transaction->commit();

                $n--;
            }catch(\Throwable $e) {
                $transaction->rollBack();

                throw new \Exception('Payout error. Error: '.$e->getMessage());
            }
        }
    }
}