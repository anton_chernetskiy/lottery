<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\web\View;

$this->title = 'Lottery';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Congratulations!</h1>

        <p>
            <?php echo Html::a('Get prize', 'lottery/prize', [
                'id' => 'get-prize',
                'class' => 'btn btn-lg btn-success',
            ])?>
        </p>
    </div>

    <div class="body-content">

        <div class="row">

        </div>

    </div>
</div>

<?php $this->registerJs("
    $('#get-prize').on('click', function(){
        var link = $(this);
        
        $.ajax({
            url: link.attr('href'),
            dataType: 'json',
            type: 'POST',
            success: function (response){
                if(response.status == 'ok'){
                    alert(response.mgs);
                }else if(response.status == 'error'){
                    alert('Error' + response.mgs);
                }else{
                    alert('Server unknown response');
                }
            },
            error:function (xhr, ajaxOptions, thrownError){
                alert('Server error');
            }
        });
        
        return false;
    });",
    View::POS_READY,
    'get-prize-button-handler'
); ?>
