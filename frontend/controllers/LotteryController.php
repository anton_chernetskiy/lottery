<?php
namespace frontend\controllers;

use common\models\Lottery;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * LotteryController controller
 */
class LotteryController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['prize'],
                'rules' => [
                    [
                        'actions' => ['prize'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionPrize()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        try{
            $logPrize = Lottery::getRandomPrizeByUserId(Yii::$app->user->id);

            return $this->asJson([
                'status' => 'ok',
                'mgs' => \Yii::t('app', 'Congratulations, you prize is {name}', [
                    'name' => $logPrize->getName()
                ])
            ]);
        }catch (\Throwable $e){
            $this->asJson([
                'status' => 'error',
                'mgs' => $e->getMessage()
            ]);
        }
    }
}