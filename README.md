1. Нужно предоставить прототип в PHP 5.6+ без использования фреймворков / или с ними, но можно использовать любые библиотеки. Где хранить данные - на ваше усмотрение.

Реализовано на основе Yii2. Шаблон advanced. Весь функционал в frontend приложении.
В интерфейсе отказ от приза не показан. В каждой модели реализованы 3 метода для каждого типа приза:
- отказ (cancel)
- вывод средств (payout)
- конвертация (convert)

2. Реализация с помощью фреймворка (можно любой, но лучше Yii или Yii2), использованием БД. Нужно
добавить консольную команду которая будет отправлять денежные призы на счета пользователей, которые
еще не были отправлены пачками по N штук.

Команда в `console/controllers/PayoutController.php`. 
Возможно предполагалось что одновременный вывод - это запрос через multi_curl?

3. Добавить юнит-тест конвертирования денежного приза в баллы лояльности.

Тест в `common/tests/unit/models/MoneyPrizeLogTest.php` метод testConvert

DIRECTORY STRUCTURE
-------------------

```
common
    config/              contains shared configurations
    mail/                contains view files for e-mails
    models/              contains model classes used in both backend and frontend
    tests/               contains tests for common classes    
console
    config/              contains console configurations
    controllers/         contains console controllers (commands)
    migrations/          contains database migrations
    models/              contains console-specific model classes
    runtime/             contains files generated during runtime
frontend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains frontend configurations
    controllers/         contains Web controller classes
    models/              contains frontend-specific model classes
    runtime/             contains files generated during runtime
    tests/               contains tests for frontend application
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
    widgets/             contains frontend widgets
vendor/                  contains dependent 3rd-party packages
environments/            contains environment-based overrides
```
